<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://alispx.ninja
 * @since      1.0.0
 *
 * @package    Wcbs
 * @subpackage Wcbs/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Wcbs
 * @subpackage Wcbs/includes
 * @author     99plugins <support@99plugins.ninja>
 */
class Wcbs_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
