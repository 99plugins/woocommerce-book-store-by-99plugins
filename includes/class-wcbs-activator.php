<?php

/**
 * Fired during plugin activation
 *
 * @link       http://alispx.ninja
 * @since      1.0.0
 *
 * @package    Wcbs
 * @subpackage Wcbs/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Wcbs
 * @subpackage Wcbs/includes
 * @author     99plugins <support@99plugins.ninja>
 */
class Wcbs_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
